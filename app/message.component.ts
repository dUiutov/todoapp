import {Component, style, HostBinding} from "@angular/core";
import {Router} from '@angular/router';

import {MessageService} from "./message.service";
import {inOutAnimation} from './route-animation';

export enum Type {Info, Success, Alert}

@Component({
    moduleId: module.id,
    templateUrl: 'message.component.html',
    animations: [inOutAnimation],
    styles: [`
        :host { 
            position: relative; 
            top: 10%; 
            left: 10px;
        }
    `]
})
export class PopupMessageComponent {
    @HostBinding('@routeAnimation') routeAnimation = true;
    @HostBinding('style.display') display = 'block';
    @HostBinding('style.position') position = 'absolute';

    constructor(private msgService: MessageService, private router: Router) {
        this.message = this.msgService.messge;
        this.type = this.msgService.type;
        if (!this.message) {
            this.router.navigate([{outlets: {popup: null}}]);
        }
    }

    private message: string;
    private type: Type;
}