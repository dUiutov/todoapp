import {Injectable} from '@angular/core';
import {Router} from "@angular/router";

import {Type} from './message.component';

@Injectable()
export class MessageService {

    constructor(private router: Router) {
    }

    messge: string;
    type: Type;

    showMessage(msg: string, type: Type = 0) {
        this.messge = msg;
        this.type = type;
        this.router.navigate([{outlets: {popup: ['popup']}}]);
        setTimeout(() => this.router.navigate([{outlets: {popup: null}}]), 5000);
    }
}