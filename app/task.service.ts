import {Injectable, OnDestroy} from '@angular/core';
import {Http} from '@angular/http';
import {Router} from "@angular/router";

import {Task, TaskInfo} from './Task';
import {MessageService} from './message.service';
import {Type} from './message.component';

import {Subject} from 'rxjs/Subject';
import {Subscription} from "rxjs/Subscription";

@Injectable()
export class TaskService implements OnDestroy {

    taskStream: Subject<boolean> = new Subject<boolean>();
    private subs: Subscription[] = [];
    tasks: Task[] = [];
    autorisation: boolean = false;


    constructor(private http: Http, private router: Router, private msgService: MessageService) {
        http.get('/authorisation').subscribe(() => {
            this.autorisation = true;
            router.navigate(['/list'])
        }, () => {
            this.autorisation = false;
            router.navigate(['/logincomp'])
        });
    }

    login(login: string, password: string) {
        this.http.post('/login', {
            login: login,
            password: password
        }).subscribe(() => {
            this.autorisation = true;
            this.router.navigate(['/']).then(() => this.msgService.showMessage('Login'));
        }, () => this.msgService.showMessage('Login failed', Type.Alert));
    }

    registration(login: string, password: string) {
        this.http.post('/registration', {
            login: login,
            password: password
        }).subscribe(() => {
            this.login(login, password);
        }, () => this.msgService.showMessage('Registration failed', Type.Alert));
    }

    logout() {
        this.http.get('/logout').subscribe(() => {
            this.autorisation = false;
            this.resetTasks();
            this.router.navigate(['/logincomp']).then(() => this.msgService.showMessage('Logout'));
        });

    }

    loadTasks() {
        this.tasks = [];

        this.http.get('/task').subscribe((val: any) => {
            let tempArr: Task[] = [];
            if (val.length != 0) {
                let items = val.json();
                for (let i = 0; i < items.length; i++) {
                    let task = new Task('');
                    task.state = items[i];
                    tempArr.push(task);
                }
            }
            this.tasks = tempArr;
            this.taskStream.next(true);
            this.msgService.showMessage('Tasks loaded')
        }, (err) => {
            if (err.status == 401) {
                this.autorisation = false;
            }
        });
    }

    saveTasks() {
        let states: TaskInfo[] = [];
        this.tasks.forEach(function (item) {
            states.push(item.state);
        });
        this.http.post('/task', states).subscribe(() => this.msgService.showMessage('Update saved', Type.Success));
    }

    clearTasks() {
        this.tasks = [];
        this.http.get('/clearTasks');
    }

    pushTask(newTask: Task) {
        this.http.post('/task', newTask.state).subscribe((response) => {
            newTask.state._id = response.json();
            this.tasks.push(newTask);
            this.taskStream.next(true);
        });
    }

    updateTask(task: Task) {
        if (!task.state._id) {
            return;
        }
        this.http.post('./updateTask', task.state).subscribe((response) => {
            let index = this.tasks.findIndex((item) => {
                return item.state._id === task.state._id;
            });
            this.tasks[index].state = task.getStateCopy();
            this.taskStream.next(true);
        })
    }

    removeTask(task: Task) {
        if (!task.state._id) {
            return;
        }
        this.http.post('/deleteTask', {_id: task.state._id}).subscribe(() => {
            let index = this.tasks.findIndex((item) => {
                return item.state._id === task.state._id;
            });
            this.tasks.splice(index, 1);
            this.taskStream.next(true);
            this.msgService.showMessage('Task deleted', Type.Success);
        });

    }

    resetTasks() {
        this.tasks = [];
        this.taskStream.next(false);
    }

    ngOnDestroy() {
        this.subs.forEach(function (item) {
            item.unsubscribe();
        });
        this.subs = [];
    }
}