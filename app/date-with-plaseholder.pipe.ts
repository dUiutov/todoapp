import {Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from "@angular/common";

@Pipe({name: 'dateWithPlaseholder'})
export class DateWithPlaseholderPipe extends DatePipe implements PipeTransform {
    transform(date: number, str?: string): string {
        if (isNaN(date) || date.valueOf() == 0) {
            return '---';
        } else {
            return super.transform(date, str);
        }
    }
}