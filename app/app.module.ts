import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {TaskListComponent}  from './task-list.component';
import {TaskService} from './task.service';
import {StatusPipe} from './status.pipe';
import {TimeIntervalPipe} from './time-interval.pipe';
import {DateWithPlaseholderPipe} from "./date-with-plaseholder.pipe";
import {DetailTaskComponent} from "./detail-task.component";
import {DateComponent} from './date.component';
import {TimeIntervalComponent} from './time-interval.component';
import {AppRouterModule} from './app-router.module';
import {LoginComponent} from './login.component';
import {AuthGuard} from "./auth-guard.service";
import {PopupMessageComponent} from "./message.component";
import {MessageService} from "./message.service";


@NgModule({
    imports: [BrowserModule,
        FormsModule,
        AppRouterModule,
        HttpModule],
    declarations: [AppComponent,
        TaskListComponent,
        StatusPipe,
        TimeIntervalPipe,
        DateWithPlaseholderPipe,
        DetailTaskComponent,
        DateComponent,
        TimeIntervalComponent,
        LoginComponent,
        PopupMessageComponent],
    bootstrap: [AppComponent],
    providers: [TaskService,
        AuthGuard,
        MessageService]
})
export class AppModule {
}
