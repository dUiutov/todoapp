export enum Status { New, InProcess, Pause, Resolved}

export class Task {

    constructor(title: string,
                description?: string,
                plannedStartDate?: Date,
                plannedEstimate?: number) {
        this.taskState = new TaskInfo(title, description, plannedStartDate, plannedEstimate);
    }

    private taskState: TaskInfo;

    spendCash: number;

    spendCashUpdate() {
        this.spendCash = this.spendTime;
    }

    get state(): TaskInfo {
        return this.taskState;
    }

    set state(newState: TaskInfo) {
        this.taskState = newState;
        this.taskState.createDate = new Date(this.taskState.createDate);
        if (this.taskState.startDate) {
            this.taskState.startDate = new Date(this.taskState.startDate);
        }
        if (this.taskState.plannedEstimate_) {
            this.taskState.plannedStartDate_ = new Date(this.taskState.plannedStartDate_);
        }
    }

    getStateCopy(): TaskInfo {
        let tempTask = new TaskInfo('');
        for (let prop in this.state) {
            tempTask[prop] = this.state[prop];
        }
        return tempTask;
    }

    get title(): string {
        return this.taskState.title;
    }

    set title(newTitile: string) {
        this.taskState.title = newTitile;
    }

    get description(): string {
        return this.taskState.description;
    }

    set description(desc: string) {
        this.taskState.description = desc;
    }

    resetCreateDate() {
        this.state.createDate = new Date;
    }

    get createDate(): Date {
        return this.taskState.createDate;
    }

    get status(): Status {
        return this.taskState.status_;
    }

    set status(newStatus: Status) {
        if (newStatus == Status.InProcess) {
            if (this.taskState.status_ == Status.New) {
                this.taskState.lastStart = this.taskState.startDate = new Date;
            } else if (this.taskState.status_ == Status.Pause || this.taskState.status_ == Status.Resolved) {
                this.taskState.lastStart = new Date;
            }
            this.taskState.status_ = newStatus;
        } else if (newStatus == Status.Pause && this.taskState.status_ == Status.InProcess) {
            this.taskState.spendTime_ = this.spendTime;
            this.taskState.status_ = newStatus;
        } else if (newStatus == Status.Resolved && this.taskState.status_ != Status.Resolved) {
            this.taskState.spendTime_ = this.spendTime;
            this.taskState.status_ = newStatus;
        }
    }

    get plannedStartDate(): Date {
        return this.taskState.plannedStartDate_;
    }

    set plannedStartDate(newPlannedDate: Date) {
        if (newPlannedDate.valueOf() > Date.now()) {
            this.taskState.plannedStartDate_ = newPlannedDate;
        } else if (newPlannedDate.valueOf() == 0) {
            this.taskState.plannedStartDate_ = new Date(0);
        } else if (newPlannedDate.valueOf() <= Date.now()) {
            newPlannedDate.setMinutes(newPlannedDate.getMinutes() + 1);
            this.taskState.plannedStartDate_ = newPlannedDate;
        }
    }

    get startDate(): Date {
        return this.taskState.startDate;
    }

    get plannedEstimate(): number {
        return this.taskState.plannedEstimate_;
    }

    set plannedEstimate(newEstimate: number) {
        this.taskState.plannedEstimate_ = newEstimate;
    }

    get spendTime(): number {
        if (this.status == Status.New) {
            return 0;
        }
        else if (this.status == Status.InProcess) {
            //let ms = Math.floor((Date.now() - new Date(this.taskState.lastStart).valueOf()) / 1000) * 1000;
            let ms = Date.now() - new Date(this.taskState.lastStart).valueOf();
            return this.taskState.spendTime_ ? ms + this.taskState.spendTime_ : ms;
        }
        else if (this.status == Status.Resolved || this.status == Status.Pause) {
            return this.taskState.spendTime_;
        }
        return 0;
    }

    clipDescription(): string {
        if (!this.taskState.description) {
            return '---';
        } else if (this.taskState.description.length > 200) {
            return this.taskState.description.substr(0, 200) + '...';
        } else {
            return this.taskState.description;
        }
    }
}

export class TaskInfo {
    constructor(title: string,
                description?: string,
                plannedStartDate?: Date,
                plannedEstimate?: number) {
        this.title = title;
        if (description) this.description = description;
        this.createDate = new Date;
        this.status_ = Status.New;
        if (plannedStartDate) this.plannedStartDate_ = plannedStartDate;
        if (plannedEstimate) this.plannedEstimate_ = plannedEstimate;
    }

    _id: any;
    title: string;
    description: string;
    createDate: Date;

    status_: Status;

    plannedStartDate_: Date;
    startDate: Date;

    plannedEstimate_: number;
    spendTime_: number;

    lastStart: Date;
}