import {Pipe, PipeTransform} from '@angular/core';
import {Status} from './Task'

@Pipe({name: 'status'})
export class StatusPipe implements PipeTransform {
    transform(status: Status): string {
        let str: string;
        switch (status) {
            case Status.New:
                str = 'New';
                break;
            case Status.InProcess:
                str = 'In process';
                break;
            case Status.Pause:
                str = 'Pause';
                break;
            case Status.Resolved:
                str = 'Resolved';
                break;
            default:
                str = 'Invalid status';
                break;
        }
        return str;
    }
}