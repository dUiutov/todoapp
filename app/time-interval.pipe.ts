import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'interval'})
export class TimeIntervalPipe implements PipeTransform {
    transform(value: number): string {
        if (isNaN(value) || value <= 0) return '---';
        let str: string = ' ';
        if (value > 1000) {
            let inter = 1000 * 60 * 60 * 24 * 30 * 12; // sec * minute * hour * day * month * year
            let res: number;
            if (value >= inter) {
                res = Math.floor(value / inter);
                str += res + 'year(s) ';
                value -= inter * res;
            }
            inter /= 12;
            if (value >= inter) {
                res = Math.floor(value / inter);
                str += res + 'month(s) ';
                value -= inter * res;
            }
            inter /= 30;
            if (value >= inter) {
                res = Math.floor(value / inter);
                str += res + 'day(s) ';
                value -= inter * res;
            }
            inter /= 24;
            if (value >= inter) {
                res = Math.floor(value / inter);
                str += res + 'hour(s) ';
                value -= inter * res;
            }
            inter /= 60;
            if (value >= inter) {
                res = Math.floor(value / inter);
                str += res + 'min ';
                value -= inter * res;
            }
            inter /= 60;
            if (value >= inter) {
                res = Math.floor(value / inter);
                str += res + 'sec';
                value -= inter * res;
            }
        } else {
            str += value + 'ms';
        }
        return str;
    }
}