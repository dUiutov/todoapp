import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DetailTaskComponent} from './detail-task.component';
import {TaskListComponent} from './task-list.component';
import {AuthGuard} from "./auth-guard.service";
import {LoginComponent} from "./login.component";
import {PopupMessageComponent} from './message.component';

const routers: Routes = [
    {path: 'popup', component: PopupMessageComponent, outlet: 'popup'},
    {path: 'logincomp', component: LoginComponent},
    {
    path: '',
    canActivate: [AuthGuard],
    children: [
        {path: 'list', component: TaskListComponent},
        {path: 'detail', component: DetailTaskComponent},
        {path: '', redirectTo: '/list', pathMatch: 'full'},
        {path: '**', component: TaskListComponent}]
}];

@NgModule({
    imports: [RouterModule.forRoot(routers)],
    exports: [RouterModule]
})
export class AppRouterModule {

}