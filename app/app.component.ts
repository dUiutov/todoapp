import {Component, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

import {TaskService} from './task.service';
import {Task, Status} from './Task';
import {FilterService} from './filter.servce';

import {Subscription} from 'rxjs/Subscription';

@Component({
    selector: 'my-app',
    moduleId: module.id,
    templateUrl: 'app.component.html',
    providers: [FilterService],
    styles: [`
        .statistic {
            font-size: 80%;
            min-width: 35%;
        }
        nav {
            opacity: 0.9;
        }
        .content {
            padding-top: 80px;
        }
        .search {
            margin-top: 6px;
        }
    `]
})
export class AppComponent implements OnDestroy{

    constructor(private taskstorage: TaskService, private router: Router, private filter: FilterService, private loc: Location) {
        this.subscr.push(this.filter.filterMod.subscribe(newVal => this.filterMod = newVal));
        this.filter.filterMod.next(true);
        this.subscr.push(this.filter.filterStream.subscribe(newVal => this.myFilter = newVal));
        this.filter.currentFilter.next(this.filter.filters[0]);
        this.subscr.push(this.filter.sortStream.subscribe(newVal => this.mySort = newVal));
        this.filter.currentSort.next(this.filter.sortCrits[2]);
        this.subscr.push(this.filter.directStream.subscribe(newVal => this.myAscending = newVal));
        this.filter.currentDirect.next(true);
        this.subscr.push(this.filter.currentSearch.subscribe(newVal => this.mySearch = newVal));
        this.filter.currentSearch.next(this.filter.sortCrits[0]);
        this.subscr.push(this.filter.currentSearchTerm.subscribe(newVal => this.searchTerm = newVal));
        this.filter.currentSearchTerm.next('');
    }

    private filterMod: boolean;
    private myFilter: string;
    private mySort: string;
    private myAscending: boolean;
    private mySearch: string = 'Title';
    private searchTerm: string;
    private subscr: Subscription[] = [];

    getStatistic(status?: Status) {
        let count: number = 0;
        if (!isNaN(status)) {
            this.taskstorage.tasks.forEach((value: Task) => {
                if (value.status == status) count++;
            });
        } else {
            count = this.taskstorage.tasks.length;
        }
        return count;
    }

    gotoCreateTask() {
        this.router.navigate(['/detail', {newTask: true}]);
    }

    ngOnDestroy() {
        this.subscr.forEach(val => val.unsubscribe());
        this.subscr = [];
    }
}