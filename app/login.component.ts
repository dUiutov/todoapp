import {Component, state, style, animate, transition, trigger} from '@angular/core';
import {TaskService} from "./task.service";

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html',
    styles: [`
        .content {
            padding-left: 30%;
            padding-right: 30%;
        }
        nav {
            z-index: 100;
        }
    `],
    animations: [trigger('loginRegistration', [
        state('in', style({transform: 'scale(1)'})),
        transition('void => *', [style({transform: 'scale(0)'}), animate(250)]),
        transition('* => void', animate(250, style({transform: 'scale(0)'})))
    ])

    ]
})
export class LoginComponent {
    private status = "login";
    private login: string;
    private password: string;
    private rePassword: string;

    constructor(private taskService: TaskService){}

    private switchStatus() {
        this.login = '';
        this.password = '';
        this.rePassword = '';
        if (this.status === 'login') {
            this.status = 'registration';
        } else {
            this.status = 'login';
        }
    }
}