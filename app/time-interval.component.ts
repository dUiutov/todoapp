import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';

@Component({
    selector: 'interval-comp',
    moduleId: module.id,
    templateUrl: 'time-interval.component.html'
})
export class TimeIntervalComponent implements OnInit {
    @Input()
    value: number;
    @Output()
    valueChange = new EventEmitter<number>();

    minutes_: number = 0;
    hours_: number = 0;
    days_: number = 0;
    months_: number = 0;
    years_: number = 0;

    valueUpdate() {
        this.value = this.years_ * 1000 * 60 * 60 * 24 * 30 * 12 + this.months_ * 1000 * 60 * 60 * 24 * 30 + this.days_ * 1000 * 60 * 60 * 24 + this.hours_ * 1000 * 60 * 60 + this.minutes_ * 1000 * 60;
        this.valueChange.emit(this.value);
    }

    ngOnInit() {
        if (this.value) {
            let inter = 1000 * 60 * 60 * 24 * 30 * 12; // sec * minute * hour * day * month * year
            let res: number;
            if (this.value >= inter) {
                res = Math.floor(this.value / inter);
                this.years_ = res;
                this.value -= inter * res;
            }
            inter /= 12;
            if (this.value >= inter) {
                res = Math.floor(this.value / inter);
                this.months_ = res;
                this.value -= inter * res;
            }
            inter /= 30;
            if (this.value >= inter) {
                res = Math.floor(this.value / inter);
                this.days_ = res;
                this.value -= inter * res;
            }
            inter /= 24;
            if (this.value >= inter) {
                res = Math.floor(this.value / inter);
                this.hours_ = res;
                this.value -= inter * res;
            }
            inter /= 60;
            if (this.value >= inter) {
                res = Math.floor(this.value / inter);
                this.minutes_ = res;
                this.value -= inter * res;
            }
        }
    }

    get minutes(): number {
        return this.minutes_;
    }

    set minutes(val: number) {
        for (; val >= 60; val -= 60) {
            this.hours += 1;
        }
        this.minutes_ = val;
        this.valueUpdate();
    }

    get hours(): number {
        return this.hours_;
    }

    set hours(val: number) {
        for (; val >= 24; val -= 24) {
            this.days += 1;
        }
        this.hours_ = val;
        this.valueUpdate();
    }

    get days(): number {
        return this.days_;
    }

    set days(val: number) {
        for (; val >= 30; val -= 30) {
            this.months += 1;
        }
        this.days_ = val;
        this.valueUpdate();
    }

    get months(): number {
        return this.months_;
    }

    set months(val: number) {
        for (; val >= 12; val -= 12) {
            this.years += 1;
        }
        this.months_ = val;
        this.valueUpdate();
    }

    get years(): number {
        return this.years_;
    }

    set years(val: number) {
        this.years_ = val;
        this.valueUpdate();
    }
}