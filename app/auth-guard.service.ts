import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';

import {TaskService} from "./task.service";



@Injectable()
export class AuthGuard implements CanActivate{

    constructor(private taskService: TaskService, private router: Router) {}

    canActivate(): boolean {
        if (this.taskService.autorisation) {
            return true;
        } else {
            this.router.navigate(['/logincomp']);
            return false;
        }
    }
}