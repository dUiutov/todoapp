import {Component, OnDestroy, trigger, state, transition, animate, style, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {TaskService} from "./task.service";
import {Task, Status, TaskInfo} from'./Task';
import {FilterService} from './filter.servce';

import {Subscription} from 'rxjs/Subscription';

@Component({
    selector: 'task-list',
    moduleId: module.id,
    templateUrl: 'task-list.component.html',
    styles: [`
         .dropdown-menu a {
            cursor: pointer;
        }
        .close:hover {
            transform: scale(1.5)
        }
        .editbtn:hover {
            transform: scale(1.2)
        }
    `],
    animations: [trigger('inputOutput', [
            state('in', style([{transform: 'translateX(0%)'}, {transform: 'scale(1)'}])),
            transition('void => *', [style({transform: 'translateX(-150%)'}), animate('300ms')]),
            transition('* => void', animate('300ms', style({transform: 'scale(0)'})))
        ]
    )]
})
export class TaskListComponent implements OnDestroy, OnInit {

    constructor(private taskstorage: TaskService, private router: Router, private filter: FilterService) {}

    ngOnInit() {
        this.subs.push(this.taskstorage.taskStream.subscribe(() => {
            this.init();
        }));
        this.timerID = setInterval(() => {
            this.tasks.forEach((task: Task) => {
                if (task.status == Status.InProcess)
                    task.spendCashUpdate();
            })
        }, 1000);

        this.init();

        this.subs.push(this.filter.filterStream.subscribe((newVal) => {
            this.currentFilter = newVal;
            this.init();
        }));
        this.subs.push(this.filter.sortStream.subscribe((newVal) => {
            this.currentSort = newVal;
            this.init();
        }));
        this.subs.push(this.filter.currentDirect.subscribe((newVal) => {
            this.ascending = newVal;
            this.init();
        }));
        this.subs.push(this.filter.currentSearch.subscribe((newVal) => {
            this.search = newVal;
            this.init();
        }));
        this.subs.push(this.filter.currentSearchTerm.subscribe((newVal) => {
            this.searchTerm = newVal.trim();
            this.init();
        }));

        this.taskstorage.loadTasks();
    }

    private timerID: any;
    subs: Subscription[] = [];
    tasks: Task[] = [];
    private currentFilter: string = 'All';
    private currentSort: string = 'CreateDate';
    private ascending: boolean = true;
    private search: string = 'Title';
    private searchTerm: string = '';

    deleteTask(delTask: Task) {
        if (confirm("Are you sure to delete this task?")) {
            this.taskstorage.removeTask(delTask);
        }
    }

    updateStatus(task: Task, status: Status) {
        let tempTask = new Task('');
        tempTask.state = task.getStateCopy();
        tempTask.status = status;
        this.taskstorage.updateTask(tempTask);
    }

    private init() {
        this.tasks = [];
        this.filtering();
        this.searching();
        this.sorting();
    }

    filtering() {
        if (this.currentFilter == 'All') {
            this.taskstorage.tasks.forEach(item => this.tasks.push(item));
            return;
        }
        if (this.currentFilter == 'New') {
            this.tasks = this.taskstorage.tasks.filter(val => val.status == Status.New);
        } else if (this.currentFilter == 'In process') {
            this.tasks = this.taskstorage.tasks.filter(val => val.status == Status.InProcess);
        } else if (this.currentFilter == 'Pause') {
            this.tasks = this.taskstorage.tasks.filter(val => val.status == Status.Pause);
        } else if (this.currentFilter == 'Resolved') {
            this.tasks = this.taskstorage.tasks.filter(val => val.status == Status.Resolved);
        }
    }

    searching() {
        if (!this.searchTerm) {
            return;
        }
        let deleting: number[] = [];
        if (this.search == 'Title') {
            this.tasks.forEach((item, index) => {
                if (item['title'].toLowerCase().indexOf(this.searchTerm.toLowerCase()) == -1) {
                    deleting.push(index);
                }
            });
        } else {
            if (this.searchTerm == '---') {
                this.tasks.forEach((item, index) => {
                    if (item['description']) {
                        deleting.push(index);
                    }
                })
            } else {
                this.tasks.forEach((item, index) => {
                    if (!item['description']) {
                        deleting.push(index);
                    } else {
                        if (item['description'].toLowerCase().indexOf(this.searchTerm.toLowerCase()) == -1) {
                            deleting.push(index);
                        }
                    }
                });
            }
        }
        deleting = deleting.reverse();
        deleting.forEach(item => this.tasks.splice(item, 1));
    }

    sorting() {
        if (this.ascending) {
            if (this.currentSort == 'Title') {
                this.tasks = this.tasks.sort((a, b) => {
                    return (a.title ? a.title : '---').localeCompare(b.title ? b.title : '---');
                });
            } else if (this.currentSort == 'Description') {
                this.tasks = this.tasks.sort((a, b) => {
                    return (a.description ? a.description : '---').localeCompare(b.description ? b.description : '---');
                });
            } else if (this.currentSort == 'CreateDate') {
                this.tasks = this.tasks.sort((a, b) => {
                    return new Date(a.createDate).valueOf() - new Date(b.createDate).valueOf();
                });
            } else if (this.currentSort == 'Status') {
                this.tasks = this.tasks.sort((a, b) => {
                    return a.status - b.status;
                });
            } else if (this.currentSort == 'PlannedStartDate') {
                this.tasks = this.tasks.sort((a, b) => {
                    return (a.createDate ? new Date(a.plannedStartDate).valueOf() : 0) - (b.plannedStartDate ? new Date(b.plannedStartDate).valueOf() : 0);
                });
            } else if (this.currentSort == 'StartDate') {
                this.tasks = this.tasks.sort((a, b) => {
                    return (a.startDate ? new Date(a.startDate).valueOf() : 0) - (b.startDate ? new Date(b.startDate).valueOf() : 0);
                });
            } else if (this.currentSort == 'PlannedEstimate') {
                this.tasks = this.tasks.sort((a, b) => {
                    return (a.plannedEstimate ? a.plannedEstimate : 0) - (b.plannedEstimate ? b.plannedEstimate : 0);
                });
            } else if (this.currentSort == 'SpendTime') {
                this.tasks = this.tasks.sort((a, b) => {
                    return (a.spendTime ? a.spendTime : 0) - (b.spendTime ? b.spendTime : 0);
                });
            }
        } else {
            if (this.currentSort == 'Title') {
                this.tasks = this.tasks.sort((a, b) => {
                    return (b.title ? b.title : '---').localeCompare(a.title ? a.title : '---');
                });
            } else if (this.currentSort == 'Description') {
                this.tasks = this.tasks.sort((a, b) => {
                    return (b.description ? b.description : '---').localeCompare(a.description ? a.description : '---');
                });
            } else if (this.currentSort == 'CreateDate') {
                this.tasks = this.tasks.sort((a, b) => {
                    return new Date(b.createDate).valueOf() - new Date(a.createDate).valueOf();
                });
            } else if (this.currentSort == 'Status') {
                this.tasks = this.tasks.sort((a, b) => {
                    return b.status - a.status;
                });
            } else if (this.currentSort == 'PlannedStartDate') {
                this.tasks = this.tasks.sort((a, b) => {
                    return (a.plannedStartDate ? new Date(a.plannedStartDate).valueOf() : 0) - (b.plannedStartDate ? new Date(b.plannedStartDate).valueOf() : 0);
                });
            } else if (this.currentSort == 'StartDate') {
                this.tasks = this.tasks.sort((a, b) => {
                    return (b.startDate ? new Date(b.startDate).valueOf() : 0) - (a.startDate ? new Date(a.startDate).valueOf() : 0);
                });
            } else if (this.currentSort == 'PlannedEstimate') {
                this.tasks = this.tasks.sort((a, b) => {
                    return (b.plannedEstimate ? b.plannedEstimate : 0) - (a.plannedEstimate ? a.plannedEstimate : 0);
                });
            } else if (this.currentSort == 'SpendTime') {
                this.tasks = this.tasks.sort((a, b) => {
                    return (b.spendTime ? b.spendTime : 0) - (a.spendTime ? a.spendTime : 0);
                });
            }
        }
    }

    gotoTask(id?: string) {
        if (id) {
            this.router.navigate(['/detail', {newTask: false, id: id}]);
        }
    }

    ngOnDestroy() {
        clearInterval(this.timerID);
        this.subs.forEach(val => val.unsubscribe());
        this.subs = [];
    }
}