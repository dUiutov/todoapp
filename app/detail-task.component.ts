import {Component, Input, OnInit, OnDestroy, trigger, style, state, transition, animate} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';

import {TaskService} from './task.service';
import {Task, TaskInfo} from './Task';
import {MessageService} from "./message.service";
import {Type} from './message.component';

import 'rxjs/add/operator/switchMap';


@Component({
    selector: 'detail-task',
    moduleId: module.id,
    templateUrl: 'detail-task.component.html',
    animations: [
        trigger('animation', [
            state('in', style([{transform: 'translateY(0%)'}, {transform: 'translateX(0%)'}])),
            transition('void => *', [style({transform: 'translateY(-150%)'}), animate(300)]),
            transition('* => void', animate(300, style({transform: 'translateX(150%)'})))
        ])
    ]
})
export class DetailTaskComponent implements OnInit, OnDestroy {

    constructor(private taskstorage: TaskService, private route: ActivatedRoute, private router: Router, private msgService: MessageService) {
    }

    ngOnInit() {
        if (!this.task) {
            this.task = new Task('');
        }

        this.route.params
            .subscribe((params: Params) => this.newTask = (params['newTask']) == 'true');

        let str: string;
        this.route.params
            .subscribe((params: Params) => str = params['id']);
        if (str) {
            this.task = this.taskstorage.tasks.find((item) => {
                return item.state._id == str
            });
            this.task.plannedStartDate = new Date(this.task.plannedStartDate);

            this.originTitle = this.task.title;
            this.originCreateDate = this.task.createDate;
        }

        if (this.newTask) {
            this.timers.push(setInterval(() => {
                this.cashCreateTime = Date.now();
            }, 1000));
        } else {
            this.cashCreateTime = new Date(this.task.createDate).valueOf();
        }

        this.timers.push(setInterval(() => {
            this.task.spendCashUpdate();
        }, 1000));

        if (this.task.plannedStartDate && this.task.plannedStartDate.valueOf()) {
            this.startTimeEnabled = true;

        } else {
            this.startTimeEnabled = false;
        }

        this.timers.push(setInterval(() => {
            if (this.startTimeEnabled && this.task.plannedStartDate.valueOf() < Date.now()) {
                this.task.plannedStartDate = new Date;
            }
        }), 60000);

        if (this.task.plannedEstimate > 0) {
            this.plannedEstimateEnabled = true;
        } else {
            this.plannedEstimateEnabled = false;
        }
    }

    @Input() newTask: boolean;
    @Input() task: Task;
    private originTitle: string;
    private originCreateDate: Date;

    private startTimeEnabled: boolean;

    private plannedStartManage() {
        if (!this.startTimeEnabled) {
            this.task.plannedStartDate = new Date();
        } else {
            this.task.plannedStartDate = new Date(0);
        }
    }

    private plannedEstimateEnabled: boolean;

    cashCreateTime: number;
    timers: any[] = [];

    save() {
        if (!this.startTimeEnabled) {
            this.task.plannedStartDate = new Date(0);
        } else if (this.task.plannedStartDate.valueOf() < Date.now()) {
            let temp = new Date();
            temp.setMinutes(temp.getMinutes() + 1);
            this.task.plannedStartDate = temp;
        }

        if (!this.plannedEstimateEnabled) {
            this.task.plannedEstimate = -1;
        }

        if (this.newTask) {
            this.task.resetCreateDate();
            this.taskstorage.pushTask(this.task);
        } else {
            this.taskstorage.updateTask(this.task);
        }
        this.router.navigate(['list']).then(() => {
                if (this.newTask) {
                    this.msgService.showMessage('Add task', Type.Success);
                } else {
                    this.msgService.showMessage('Update saved', Type.Success);
                }
            }
        );
    }

    ngOnDestroy() {
        this.timers.forEach((item) => clearInterval(item));
        this.timers = [];
    }
}