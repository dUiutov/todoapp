import {animate, AnimationEntryMetadata, state, style, transition, trigger} from '@angular/core';

export const inOutAnimation: AnimationEntryMetadata =
    trigger('routeAnimation', [
            state('*', style([{transform: 'translateY(0%)', opacity: 1}])),
            transition('void => *', [style({transform: 'translateY(-250%)'}), animate('500ms ease-in')]),
            transition('* => void', animate(1000, style({opacity: 0})))
        ]
    );