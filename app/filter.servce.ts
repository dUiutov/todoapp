import {Injectable} from '@angular/core'

import {Subject} from 'rxjs/Subject';

@Injectable()
export class FilterService {
    filterMod = new Subject<boolean>();

    filters = ['All', 'New', 'In process', 'Pause', 'Resolved'];
    currentFilter = new Subject<string>();
    filterStream = this.currentFilter.asObservable();

    currentDirect = new Subject<boolean>();
    directStream = this.currentDirect.asObservable();

    sortCrits = ['Title', 'Description', 'CreateDate', 'Status', 'PlannedStartDate', 'StartDate', 'PlannedEstimate', 'SpendTime'];
    currentSort = new Subject<string>();
    sortStream = this.currentSort.asObservable();

    currentSearch = new Subject<string>();

    currentSearchTerm = new Subject<string>();
}