import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';

@Component({
    selector: 'date-comp',
    moduleId: module.id,
    templateUrl: 'date.component.html'
})
export class DateComponent implements OnInit {
    constructor() {
    }

    ngOnInit() {
        if (this.value) {
            this.value = new Date(this.value);
            this.hour_ = this.value.getHours();
            this.minute_ = this.value.getMinutes();
            this.day_ = this.value.getDate();
            this.month_ = this.value.getMonth() + 1;
            this.year_ = this.value.getFullYear();
        }
    }

    @Input()
    value: Date;
    @Output()
    valueChange = new EventEmitter<Date>();
    private hour_: number = 0;
    private minute_: number = 0;
    private day_: number = 1;
    private maxDay: number = 31;
    private month_: number = 1;
    private year_: number = 1970;

    private valueUpdate() {
        this.value = new Date(this.year_, this.month_ - 1, this.day_, this.hour_, this.minute_);
        this.valueChange.emit(this.value);
    }

    get hour(): number {
        return this.hour_;
    }

    set hour(newHour: number) {
        if (newHour > 23) {
            newHour = 23;
        } else if (newHour < 0) {
            newHour = 0;
        }
        this.hour_ = newHour;
        this.valueUpdate();
    }

    get minute(): number {
        return this.minute_;
    }

    set minute(newMinute: number) {
        if (newMinute > 59) {
            newMinute = 59;
        } else if (newMinute < 0) {
            newMinute = 0;
        }
        this.minute_ = newMinute;
        this.valueUpdate();
    }

    get day(): number {
        return this.day_;
    }

    set day(newDay: number) {
        if (this.month_ == 1 || this.month_ == 3 || this.month_ == 5 || this.month_ == 7 || this.month_ == 8 || this.month_ == 10 || this.month_ == 12) {
            this.maxDay = 31;
        } else if (this.month_ == 4 || this.month_ == 6 || this.month_ == 9 || this.month_ == 11) {
            this.maxDay = 30;
        } else if (this.month_ == 2) {
            if ((this.year_ / 4) - Math.floor(this.year_ / 4)) {
                this.maxDay = 28;
            } else {
                this.maxDay = 29;
            }
        }
        if (newDay > this.maxDay) {
            newDay = this.maxDay;
        } else if (newDay < 1) {
            newDay = 1;
        }
        this.day_ = newDay;
        this.valueUpdate();
    }

    get month(): number {
        return this.month_;
    }

    set month(newMonth: number) {
        if (newMonth > 12) {
            newMonth = 12;
        } else if (newMonth < 1) {
            newMonth = 1;
        }
        this.month_ = newMonth;
        this.day = this.day_;
    }

    get year(): number {
        return this.year_;
    }

    set year(newYear: number) {
        this.year_ = newYear;
        this.day = this.day_;
    }
}